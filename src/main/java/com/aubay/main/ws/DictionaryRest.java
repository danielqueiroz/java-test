package com.aubay.main.ws;

import java.util.List;

public interface DictionaryRest {
    List<String> startBy(String text);
    String findByWord(String word);
    void save(String newWord);
    void update(String oldWord, String newWord);
    void delete(String word);
}
