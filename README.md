# Aubay Java Performance Test

## The Main Idea
There is a old way to get the dictionary words from the storage, the words are returned 
in the same list. The product owner changed the requirements and now we need to split 
these words in different lists, split by the first letter of the word. Through that
we will optimize the search logic to find the word faster than old logic.

## Functional requirements
1. We should find a word in dictionary ignoring the accents.
1. We should ignore the text case sensitive.
1. We should not create a list to a words with accents, when the first letter has accent 
we should to storage in the same list of the words that has not. Example: The word `�gua`
should be in the same list that word `a�o`.

## Non functional requirements
1. We should use Java 8 or more. 
1. We should use singleton design pattern (please, do not use framework).
1. We should read the dictionary words file when the application will be starting.
1. We should not add, delete or update words in dictionary words file, 
the methods `add`, `delete` and `update` should change only the object in memory.
When the application will be restarted these information will be lost.   
1. The development team can use any framework in REST layer. 
1. The project should be auto executable (it should run without application server).

### Interface DictionaryAdapter.java
It is responsible to read the dictionary words file.

### Interface DictionaryRepository.java
It is responsible to manipulate the dictionary words. 

### Interface DictionaryRest.java
It is a webservice implementation.

> Please, afterwards also improve this document to show how to setup and execute the project.